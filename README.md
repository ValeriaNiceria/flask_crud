# CRUD Flask

```sh
pipenv shell

pipenv install flask

mkdir app

touch app/__init__.py
```

## Como rodar o projeto

```sh
export FLASK_APP=app

flask run

export FLASK_ENV=Development
export FLASK_DEBUG=True
```

```sh
pipenv install -d requests ipdb

ipython

from requests import get

get('http://localhost:5000')
```

```sh
pipenv install flask-sqlalchemy

pipenv install flask-migrate
```